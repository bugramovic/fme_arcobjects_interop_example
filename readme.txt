a small working example on how to get features out of the ArcGIS interoperability extension via arcgis-engine/arcobjects.
The class com.alta4.interop.ArrayRowBuffer is used to store the feature data,
com.alta4.interop.FMEReader actually reads the data and puts it into a List<ArrayRowBuffer>.

To test this use com.alta4.interop.App and change the path to your .fdl files 
( the variables pathToFdlFiles and fdlFile )