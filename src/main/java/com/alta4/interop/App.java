package com.alta4.interop;

import com.esri.arcgis.datainterop.FMEDatasetHelper;
import com.esri.arcgis.datainterop.FMEWorkspaceFactory;
import com.esri.arcgis.datainterop.FMEWorkspaceHelper;
import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.interop.AutomationException;
import com.esri.arcgis.system.*;
import java.io.IOException;
import java.io.PrintStream;
import java.net.UnknownHostException;
import java.util.List;

/**
 *
 */
public class App {

    public static void main(String[] args) throws AutomationException, IOException {

        App t = new App();
        try {
            t.testFME();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    final String pathToFdlFiles = 
                "C:\\Users\\abajramovic\\AppData\\Roaming\\Safe Software\\Interoperability";
    //final String dataset = "local_interop.fdl";
    final String fdlFile = "lu.fdl";

    public App() {
        EngineInitializer.initializeVisualBeans();
        initializeArcGISLicenses();
    }

    public void initializeArcGISLicenses() {
        try {
            AoInitialize ao = new AoInitialize();
            if (ao.isProductCodeAvailable( esriLicenseProductCode.esriLicenseProductCodeEngine )
                    == esriLicenseStatus.esriLicenseAvailable) {
                ao.initialize( esriLicenseProductCode.esriLicenseProductCodeEngine );
            } else if (ao.isProductCodeAvailable( esriLicenseProductCode.esriLicenseProductCodeArcInfo )
                    == esriLicenseStatus.esriLicenseAvailable) {
                System.out.println( "ArcInfo!" );
                ao.initialize( esriLicenseProductCode.esriLicenseProductCodeArcInfo );

            } else if (ao.isProductCodeAvailable( esriLicenseProductCode.esriLicenseProductCodeArcEditor )
                    == esriLicenseStatus.esriLicenseAvailable) {
                System.out.println( "ArcEditor!" );
                ao.initialize( esriLicenseProductCode.esriLicenseProductCodeArcEditor );
            } else if (ao.isProductCodeAvailable( esriLicenseProductCode.esriLicenseProductCodeArcView )
                    == esriLicenseStatus.esriLicenseAvailable) {
                System.out.println( "ArcView!" );
                ao.initialize( esriLicenseProductCode.esriLicenseProductCodeArcView );
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println( " Program Exit: Unable to check out proper licenses" );
            System.exit( 0 );
        }
    }


    public void testFME() throws UnknownHostException, IOException {
        
        final int featureClassIdx = 0;
        
        PrintStream out = System.out;
        FMEWorkspaceFactory fmeWorkspaceFactory = new FMEWorkspaceFactory();
        IPlugInWorkspaceHelper plugInWorkspaceHelper = fmeWorkspaceFactory.openWorkspace( pathToFdlFiles );
        FMEWorkspaceHelper fmeWorkspaceHelper = new FMEWorkspaceHelper( plugInWorkspaceHelper );
        printDatasets( fmeWorkspaceHelper );

        IPlugInDatasetHelper dsHelper = fmeWorkspaceHelper.openDataset( fdlFile );
        FMEDatasetHelper fmeDatasetHelper = new FMEDatasetHelper( dsHelper );
        
        printClasses( fmeDatasetHelper );
       
        out.println( "Class-name:" + fmeDatasetHelper.getClassName( featureClassIdx ) );
        IFields fields = fmeDatasetHelper.getFields( featureClassIdx );
        printFields( fields );

        FMEReader reader = new FMEReader( fmeDatasetHelper );
        List<ArrayRowBuffer> lst = reader.readFeatures( featureClassIdx );
        for (ArrayRowBuffer el : lst) {
            out.println( el );
        }
    }

    private static void printFields(IFields fields) throws AutomationException, IOException {
        int fieldsCount = fields.getFieldCount();
        String[] fieldNames = new String[fieldsCount];
        for (int i = 0; i < fieldsCount; i++) {
            String fieldName = fields.getField( i ).getName();
            System.out.println( i + " fieldName = " + fieldName + " editable=" + fields.getField( i ).isEditable() );
            fieldNames[i] = fieldName;
        }
    }
    
    private void printDatasets(FMEWorkspaceHelper fmeWorkspaceHelper) throws IOException {
        IArray array = fmeWorkspaceHelper.getDatasetNames( esriDatasetType.esriDTAny );
        for (int i = 0; i < array.getCount(); i++) {
            FMEDatasetHelper datasetHelper = new FMEDatasetHelper( array.getElement( i ) );
            System.out.println( "fmeDatasetHelper.getLocalDatasetName() = " + datasetHelper.getLocalDatasetName() );
        }
    }
    
    private void printClasses(FMEDatasetHelper fmeDatasetHelper) throws IOException {
        int classCount = fmeDatasetHelper.getClassCount();
        for (int i = 0; i < classCount; i++) {
            String className = fmeDatasetHelper.getClassName( i );
            int classIndex = fmeDatasetHelper.getClassIndex( className );
            System.out.println( "className = " + className + "\n\tclassIndex = " + classIndex );
        }
    }
}
