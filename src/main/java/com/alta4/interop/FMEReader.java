/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alta4.interop;

import com.esri.arcgis.datainterop.FMEDatasetHelper;
import com.esri.arcgis.geodatabase.IField;
import com.esri.arcgis.geodatabase.IFields;
import com.esri.arcgis.geodatabase.IPlugInCursorHelper;
import com.esri.arcgis.geometry.*;
import com.esri.arcgis.interop.AutomationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads data from the given FMEDatasetHelper/FeatureClass
 * 
 * @author abajramovic
 */
public class FMEReader {
    
    private FMEDatasetHelper dsHelper;
    
    public FMEReader(FMEDatasetHelper dsHelper) {
        this.dsHelper = dsHelper;
    }
    
    
    public List<ArrayRowBuffer> readFeatures(int featureClassIndex) throws IOException, AutomationException {
        IFields fields = dsHelper.getFields( featureClassIndex );
        int geometryType = geometryType( fields );
        // not sure about the fieldMap (last parameter -> new long[0]) .. 
        // seems to be ignored, as far as i can tell
        IPlugInCursorHelper cur = dsHelper.fetchAll( featureClassIndex, "", new long[0] );
        List<ArrayRowBuffer> result = new ArrayList<ArrayRowBuffer>();
        while( !cur.isFinished() ) {
            ArrayRowBuffer row = new ArrayRowBuffer( fields );
            cur.queryValues( row ); //sets the attributes
            
            IGeometry geom = createGeometry( geometryType ); //sets
            if (geom != null) {
                cur.queryShape( geom); 
                row.setShape( geom );
            }
            
            try { cur.nextRecord(); } catch (Exception ex) { /*ignore for now*/ }
            
            result.add(row);
        }
        return result;
    }
    
    /**
     * creates the correct geometry-instance of the given esriGeometryType
     * TODO: currently only handles point, polygon, polyline
     * @param geometryType
     * @return a geometry instance or null, if the geometryType is not recognized/handled
     */
    private IGeometry createGeometry(int geometryType) throws IOException {
        if (geometryType == esriGeometryType.esriGeometryPoint) {
            return new Point();
        } else if (geometryType == esriGeometryType.esriGeometryPolygon) {
            return new Polygon();
        } else if (geometryType == esriGeometryType.esriGeometryPolyline) {
            return new Polyline();
        } else {
            return null;
        }
    }
    
    private int geometryType(IFields fields) throws IOException, AutomationException {
        int result = esriGeometryType.esriGeometryAny;
        for(int i=0; i < fields.getFieldCount();i++) {
            IField f = fields.getField( i );
            if (f.getGeometryDef() != null) {
                result = f.getGeometryDef().getGeometryType();
            }
        }
        return result;
    }
    
}
