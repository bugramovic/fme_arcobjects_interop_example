/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alta4.interop;

import com.esri.arcgis.geodatabase.*;
import com.esri.arcgis.geometry.IGeometry;
import com.esri.arcgis.interop.AutomationException;
import java.io.IOException;

/**
 * A simple implementation of IRowBuffer, that holds it's data in an array
 * 
 * usefull to get data out of a IPlugInCursorHelper instance
 * 
 * @author abajramovic
 */
public class ArrayRowBuffer implements IRowBuffer {

    private IFields fields;
    private Object[] data;
    private IGeometry shape;
    
    /**
     * creates a SimpleRowBuffer based on the given fields (those don't have to be mutable,
     * the fields are copied and set mutable)
     * 
     * @param fields 
     */
    public ArrayRowBuffer(IFields fields) {
        try {
            Fields fieldsCopy = new Fields();
            for(int i=0; i< fields.getFieldCount();i++) {
                IField f=  fields.getField( i );
                Field mutable = new Field();
                //make sure the field is marked editable, 
                //so IPlugInCursorHelper.queryValues behaves corretly
                mutable.setEditable( true);
                
                mutable.setName( f.getName() );
                mutable.setLength( f.getLength() );
                //mutable.setGeometryDefByRef( f.getGeometryDef());
                mutable.setScale( f.getScale() );
                mutable.setType( f.getType() );
                mutable.setDefaultValue( f.getDefaultValue() );
                //...TODO: copy all field-parameters
                
                fieldsCopy.addField( mutable);
            }
            this.fields = fieldsCopy;
            this.data = new Object[fields.getFieldCount()];
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
    
    /**
     * 
     * @param i
     * @return may return null, if we don't have data at the given index
     * @throws IOException
     * @throws AutomationException 
     */
    @Override
    public Object getValue(int i) throws IOException, AutomationException {
        if (data.length > i) {
            return data[i];
        } else {
            return null;
        }
    }

    @Override
    public void setValue(int i, Object o) throws IOException, AutomationException {
        data[i] = o;
    }

    @Override
    public IFields getFields() throws IOException, AutomationException {
        return this.fields;
    }

    public IGeometry getShape() { return shape; }
    public void setShape(IGeometry shape) { this.shape = shape; }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder("ArrayRowBuffer data:");
        for(Object o : data) {
            b.append(  o  );
            b.append( " " );
        }
        b.append( "/ shape: ");
        b.append( shape);
        return b.toString();
    }
    
}
